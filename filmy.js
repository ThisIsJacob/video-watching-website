var fs=require('fs');
var http = require('http');
const { exec } = require("child_process");
const express = require("express");
const app = express();

var soubor_filmy="video.mp4";
var id_filmu="10";
var lokace_videa='ft.mp4';
var presenter_time=5;
var is_paused=false;
var admin_pass="1234";

//Konfigurace
var PORT_WEBU = 8050;
//Konfigurace

http.createServer(function (req, res) {
    //console.log(req.headers);
    console.log(req.url);
    odpovez_na_req(req, res);
}).listen(PORT_WEBU); //the server object listens on port 8080

function makeid(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function odpovez_na_req(req, response){
    if(req.url=="/verify_key"){
        if(req.headers.key){
            if(req.headers.key==id_filmu){
                response.write("true");
            }else{
                response.write("false")
            }
            response.end();
        }
        else{
            response.write("no_key");
            response.end();
        }
    }else if(req.url=="/"){
        fs.readFile('web/index.html', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        }); 

    }else if(req.url=="/get_presenter_time"){
        if(req.headers.key){
            if(req.headers.key==id_filmu){
                var ddata = JSON.stringify({
                    time: presenter_time,
                    ispaused: is_paused
                })
                response.write(ddata);
            }else{response.write("false")}
            response.end();
        }
        else{
            response.write("no_key");
            response.end();
        }
    }else if(req.url=="/jquery.js"){
        fs.readFile('web/jquery.js', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        }); 

    }else if(req.url=="/generate_key"){
        if(req.headers.adminpass && req.headers.videofile){
            if(req.headers.adminpass==admin_pass){
                lokace_videa=req.headers.videofile;
                id_filmu = makeid(10);
                console.log("Aktualni id filmu: "+id_filmu);
                response.write(id_filmu);
            }else{
                response.write("false");
            }
        }else{
            response.write("false");
        }
        response.end();
    }else if(req.url=="/admin.html"){
        fs.readFile('web/admin.html', function (err,data) {
            if (err) {
              return console.log(err);
            }
            response.write(data);
            response.end();
        }); 
    }else if(req.url=="/set_presenter_time"){
        if(req.headers.adminpass && req.headers.videopass && req.headers.videotime && req.headers.is_paused){
            if(req.headers.adminpass==admin_pass && req.headers.videopass==id_filmu){
                presenter_time = req.headers.videotime;
                if(req.headers.is_paused==1){
                    is_paused = true;
                }else{
                    is_paused = false;
                }
                response.write("true");
            }else{
                response.write("false");
            }
        }else{
            response.write("false");
        }
        response.end();
    }
}

app.get("/video.mp4", function (req, res) {
  // Ensure there is a range given for the video
  if(req.query.key!=id_filmu){
      res.status(400).send("You need to have right access key");
  }
  const range = req.headers.range;
  if (!range) {
    res.status(400).send("Requires Range header");
  }
  const videoPath = lokace_videa;
  const videoSize = fs.statSync(lokace_videa).size;
  const CHUNK_SIZE = 10 ** 6; // 1MB
  const start = Number(range.replace(/\D/g, ""));
  const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

  // Create headers
  const contentLength = end - start + 1;
  const headers = {
    "Content-Range": `bytes ${start}-${end}/${videoSize}`,
    "Accept-Ranges": "bytes",
    "Content-Length": contentLength,
    "Content-Type": "video/mp4",
  };

  // HTTP Status 206 for Partial Content
  res.writeHead(206, headers);

  // create video read stream for this particular chunk
  const videoStream = fs.createReadStream(videoPath, { start, end });

  // Stream the video chunk to the client
  videoStream.pipe(res);
});

app.listen(8000, function () {
  console.log("Listening on port 8000!");
});